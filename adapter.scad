/*
2019 David DiPaola. Licensed under CC0 (public domain).

Print settings:
    100% infill

Requires the following hardware:
    2x M3x8mm machine screws
*/
module adapter(screw_distance_y=11, tierod_screw_distance_x=11) {
    _alot = 1000;
    horn_screw_d = 3;
    tierod_screw_d = 2;
    plate_margin = [2, 2];
    plate = [
        tierod_screw_distance_x + ((tierod_screw_d/2)*2) + (plate_margin.x*2),
        screw_distance_y        + ((horn_screw_d  /2)*2) + (plate_margin.y*2),
        3
    ];

    module hole_horn() {
        translate([0, screw_distance_y/2, 0]) {
            cylinder(d=horn_screw_d, h=_alot);
        }
    }
    module hole_tierod() {
        translate([tierod_screw_distance_x/2, -screw_distance_y/2, 0]) {
            cylinder(d=tierod_screw_d, h=_alot);
        }
    }
    difference() {
        translate([-plate.x/2, -plate.y/2, 0]) {
            difference() {
                cube(plate);
                
                angle = 70;  // TODO calculate this angle
                translate([0, horn_screw_d + (plate_margin.y*2), 0]) {
                    rotate([0,0, angle]) {
                        cube([_alot,_alot,_alot]);
                    }
                    translate([plate.x, 0,0]) {
                        rotate([0,0, 90-angle]) {
                            cube([_alot,_alot,_alot]);
                        }
                    }
                }
            }
        }
        
        hole_horn();
        mirror([0, 1, 0]) hole_horn();
        
        hole_tierod();
        mirror([1, 0, 0]) hole_tierod();
    }
}

$fn = 32;
adapter();
