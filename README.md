# CW01_servo-horn-adapter

This part, along with a matching servo horn, adds over 50% of length to the servo horn.
This allows the Lunchbox (or Pumpkin) to have a much smaller turning diameter (provided the driver can even keep the front wheels on the ground 😆).

## Installation
<ol>
	<li>Print the adapter.<ul>
		<li>3D printer settings:<ul>
			<li>Infill: 100%</li>
		</ul></li>
		<li>Required hardware:<ul>
			<li>2x M3x8mm machine screws</li>
		</ul></li>
	</ul></li>
	<li>Remove lower arm plate.</li>
	<li>Disconnect tie rods from stock servo saver.</li>
	<li>Remove stock servo saver. (The servo should have nothing attached after this step.)</li>
	<li>Screw this adapter plate onto the aluminum servo horn as shown below:<ul>
		<li><img width="10%" align="middle" src="./image/0.jpg" alt="Top side of servo horn."></img></li>
		<li><img width="10%" align="middle" src="./image/1.jpg" alt="Bottom side of servo horn."></img></li>
	</ul></li>
	<li>Shorten the tie rods as follows:<ul>
		<li>Short rod length (center-to-center): ~57.5mm (Stock: 60mm)</li>
		<li>Long rod length (center-to-center): ~74.5mm (Stock: 78mm)</li>
	</ul></li>
	<li>Connect tie rods to new servo horn assembly.</li>
	<li>Attach new servo horn assembly to servo.<ul>
		<li><img width="50%" align="middle" src="./image/2.jpg" alt="Servo horn installed."></img></li>
	</ul></li>
	<li>Re-install lower arm plate.<ul>
		<li><img width="50%" align="middle" src="./image/3.jpg" alt="Fully assembled."></img></li>
	</ul></li>
	<li>Be extra careful not to flip over your Lunchbox with all this extra steering 😉.</li>
</ol>

